# BasicAlgorithm

Ege Üniversitesi IEEE EdSoc .NET ile Algoritmaya Giriş

28 Kasım 2020

## Instructors

Gürcan Biçer - <gurcan[@]gmail.com>

Mesut Soylu - <mesut[@]mesutsoylu.com>

## Suggestions

### Resources

- [r/learnprogramming](https://www.reddit.com/r/learnprogramming/wiki/faq#wiki_where_do_i_find_good_learning_resources.3F)
- [C# Tutorials](https://docs.microsoft.com/en-us/dotnet/csharp/tutorials/)
- [Microsoft Learn](https://docs.microsoft.com/en-us/learn/)
- [The C# Yellow Book - Rob Miles](https://www.robmiles.com/s/CSharp-Book-2019-Refresh.pdf)
- [Code Complete - Steve McConnell](https://www.amazon.com/Code-Complete-Practical-Handbook-Construction/dp/0735619670)
- [Clean Code - Robert C. Martin](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)
- [The Tao Of Programming](http://www.textfiles.com/100/taoprogram.pro)

### Practice

- [LeetCode](https://leetcode.com/)
- [Project Euler](https://projecteuler.net/archives)
- [HackerRank](https://www.hackerrank.com/)

### Video

- [Çizgi-Tagem e-Kampüs](https://www.cizgi-tagem.org/e-kampus-egitim/)
- [Channel 9](https://channel9.msdn.com/)

## Question

### Günlük Covid Sayısı Takip Aracı

Son teslim tarihinden önce herhangi birimize mail ile yollayabilirsiniz.

Son teslim tarihi 06.12.2020 23:59

#### Yapacakları

- Kullanıcıdan tarih alacak
- Random bir sayı vaka sayısı verecek
  - hasta sayısı
  - vaka sayısı
  - tarihin mevsimine dikkat edecek
    - Kış mevsiminde 28000 adedin altında vaka olamaz
    - Yaz mevsiminde 5000 adedin üzerinde vaka olamaz
    - Bahar 5000 - 28000 adet aralığında vaka olabilir
- Tarih ilk defa girildiği zaman rastgele bir vaka ve hasta sayısı söylesin
- Aynı tarih tekrar girildiği zaman aynı sayıları söylesin
- "Toplam" --> Toplam hasta ve vaka sayısını söylesin
  - Kullanıcının girdiği tarihlerin toplamı
  - Eğer kullanıcı hiç tarih girmediyse son 1 hafta için rastgele değer oluşturup onun toplamı alınsın
- "Ortalama" --> Ortalama hasta ve vaka sayısını söylesin
  - Kullanıcının girdiği tarihlerin ortalaması
  - Eğer kullanıcı hiç tarih girmediyse son 1 hafta için rastgele değer oluşturup onun ortalaması alınsın