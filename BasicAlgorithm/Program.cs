﻿using System;
using System.Collections.Generic;

namespace BasicAlgorithm
{
    class Program
    {


        //        C# type keyword	.NET type
        //bool System.Boolean
        //byte System.Byte
        //sbyte System.SByte
        //char System.Char
        //decimal System.Decimal
        //double System.Double
        //float System.Single
        //int System.Int32
        //uint System.UInt32
        //long System.Int64
        //ulong System.UInt64
        //short System.Int16
        //ushort System.UInt16
        //The following table lists the C# built-in reference types:

        //TABLE 2
        //C# type keyword	.NET type
        //object System.Object
        //string System.String
        //dynamic System.Object
        static void Main(string[] args)
        {

            List<string> YapilabileceklerListesi = new List<string>();

            YapilabileceklerListesi.Add("ikidegeraliptoplayanmetod");
            YapilabileceklerListesi.Add("ikidegeralipbirincidenikinciyicikartanfonksiyon");
            YapilabileceklerListesi.Add("ikidegeralipcarpistiranfonksiyon");
            YapilabileceklerListesi.Add("ikidegeralipbolusturenfonksiyon");
            YapilabileceklerListesi.Add("sayiyikontrolet");
            YapilabileceklerListesi.Add("kareninalanivecevresi");
            YapilabileceklerListesi.Add("ikidegeralarasindansalla");



            //int index = 1;
            //foreach (var YapilabilecekSey in YapilabileceklerListesi)
            //{
            //    Console.WriteLine(index.ToString() + " --> " + YapilabilecekSey);
            //    index++;
            //    //index = index + 1;
            //    //index += 1;
            //}

            for (int i = 1; i <= YapilabileceklerListesi.Count; i++)
            {
                Console.WriteLine(i.ToString() + " --> " + YapilabileceklerListesi[i - 1]);
            }



            Console.WriteLine("Yukardakilerden birinin başındaki sayıyı yazıp enter'a basınız.");

            var okunanDeger = Console.ReadLine();
            int intokunanDeger = int.Parse(okunanDeger);
            switch (intokunanDeger)
            {
                case 1:
                    Console.WriteLine("Girdiğiniz Sayıların toplamı : " + ikidegeraliptoplayanmetod().ToString());
                    break;
                case 2:
                    Console.WriteLine("ilk sayıdan ikinci sayı çıkınca, oluşan netice : " + ikidegeralipbirincidenikinciyicikartanfonksiyon().ToString());

                    break;
                case 3:
                    Console.WriteLine("iki sayı çarpıştırılınca, oluşan netice : " + ikidegeralipcarpistiranfonksiyon().ToString());
                    break;
                case 4:
                    Console.WriteLine("iki sayıyı bölüştürünce, oluşan netice : " + ikidegeralipbolusturenfonksiyon().ToString());
                    break;
                case 5:
                    sayiyikontrolet();
                    break;
                case 6:
                    Console.WriteLine(kareninalanivecevresi());
                    break;
                case 7:
                    Console.WriteLine("girilen iki değer arasında rastgele bir sayı sallamaca, neticesi : " + ikidegeralarasindansalla().ToString());
                    break;
                default:
                    Console.WriteLine("yok öyle bişey!");
                    break;
            }



            //var toplam = ikidegeraliptoplayanmetod();
            //Console.WriteLine("Girdiğiniz Sayıların toplamı : " + toplam.ToString());

            //Console.WriteLine("Girdiğiniz Sayıların toplamı : " + ikidegeraliptoplayanmetod().ToString());

            //Console.WriteLine("ilk sayıdan ikinci sayı çıkınca, oluşan netice : " + ikidegeralipbirincidenikinciyicikartanfonksiyon().ToString());

            //Console.WriteLine("iki sayı çarpıştırılınca, oluşan netice : " + ikidegeralipcarpistiranfonksiyon().ToString());

            //Console.WriteLine("iki sayıyı bölüştürünce, oluşan netice : " + ikidegeralipbolusturenfonksiyon().ToString());

            //sayiyikontrolet();

            //Console.WriteLine(kareninalanivecevresi());

            /*girilen iki değer arasında rastgele bir sayı sallamaca*/
            //Console.WriteLine("girilen iki değer arasında rastgele bir sayı sallamaca, neticesi : " + ikidegeralarasindansalla().ToString());





            Console.WriteLine("Programdan çıkmak için herhangi bir tuşa basınız.");
            Console.ReadKey();
        }

        /*Fonksiyon*/
        static int ikidegeraliptoplayanmetod()
        {
            int ilkSayi;
            int ikinciSayi;
            int toplam;

            Console.WriteLine("Bir sayı giriniz : ");
            ilkSayi = int.Parse(Console.ReadLine());
            //Console.WriteLine("Az önce girdiğiniz sayı : " + ilkSayi.ToString());


            Console.WriteLine("Yeni bir sayı giriniz : ");
            ikinciSayi = int.Parse(Console.ReadLine());
            //Console.WriteLine("Az önce ikinci olarak girdiğiniz sayı : " + ikinciSayi.ToString());


            toplam = ilkSayi + ikinciSayi;
            return toplam;

        }
        static int ikidegeralipbirincidenikinciyicikartanfonksiyon()
        {
            int ilkSayi = 0;
            int ikinciSayi = 0;
            int netice = 0;

            int integerDegisken = default; /*  0  */

            Console.WriteLine("Bir sayı gir! ");


            //try
            //{
            //    ilkSayi = int.Parse(Console.ReadLine());
            //}
            //catch (Exception ex)
            //{

            //    Console.WriteLine("Hata oluştu program kapatılacak , oluşan hata : " + ex.Message);
            //    Environment.Exit(1);
            //}

            //var intOlarakParseOldumu = int.TryParse(Console.ReadLine(), out var ilkDeger);

            //if (intOlarakParseOldumu)
            //    ilkSayi = ilkDeger;
            //else
            //{
            //    Console.WriteLine("Hata oluştu program kapatılacak");
            //    Environment.Exit(1);
            //}

            if (int.TryParse(Console.ReadLine(), out var ilkDeger))
            {
                ilkSayi = ilkDeger;
            }
            else
            {
                Console.WriteLine("Hata oluştu program kapatılacak");
                Environment.Exit(1);
            }



            Console.WriteLine("ikinci sayıyı gir! ");
            ikinciSayi = int.Parse(Console.ReadLine());

            netice = ilkSayi - ikinciSayi;

            return netice;


        }
        static int ikidegeralipcarpistiranfonksiyon()
        {
            int ilkDeger;
            int ikinciDeger;
            int netice;

            Console.WriteLine("İlk Değeri giriniz");
            ilkDeger = int.Parse(Console.ReadLine());

            Console.WriteLine("İkinci Değeri giriniz");
            ikinciDeger = int.Parse(Console.ReadLine());

            netice = ilkDeger * ikinciDeger;

            return netice;

        }
        static decimal ikidegeralipbolusturenfonksiyon()
        {
            decimal ilkDeger;
            decimal ikinciDeger;
            decimal netice;

            Console.WriteLine("İlk değeri giriniz:");
            ilkDeger = decimal.Parse(Console.ReadLine());

            Console.WriteLine("ikinci değeri girin : ");
            ikinciDeger = decimal.Parse(Console.ReadLine());

            netice = ilkDeger / ikinciDeger;

            //return (int)netice;
            return netice;

        }
        static void sayiyikontrolet()
        {
            int kontroledileceksayi = 0;
            Console.WriteLine("Uygulamadan çıkmak için yeter yazın!");
            Console.WriteLine("Kontrol sayısını girin : "); /* 100 */
            kontroledileceksayi = int.Parse(Console.ReadLine());

            string geciciDeger = "";

            while (geciciDeger != "yeter")
            {
                Console.WriteLine("kontrol edilmesi için yeni değer girin: ");
                geciciDeger = Console.ReadLine();

                if (int.TryParse(geciciDeger, out var intgecicideger))
                {
                    // kontrol edilecek sayı ile kontrol edip ekrana yazmak.
                    if (kontroledileceksayi == intgecicideger)
                    {
                        Console.WriteLine("Girilen deger : " + intgecicideger.ToString() + " kontrol değeri : " + kontroledileceksayi.ToString() + " eşit");
                    }
                    else if (kontroledileceksayi > intgecicideger)
                    {
                        Console.WriteLine(">");
                    }
                    else if (kontroledileceksayi < intgecicideger)
                    {
                        Console.WriteLine("<");
                    }
                }

            }




        }
        static string kareninalanivecevresi()
        {
            int olcu;
            Console.WriteLine("Karenin bir kenarının ölçüsünü girin :");
            olcu = int.Parse(Console.ReadLine());

            string returnValue;

            returnValue = "Karenin alanı : " + (olcu * olcu).ToString() + " Karenin çevresi : " + (olcu * 4).ToString();

            return returnValue;

        }
        static int ikidegeralarasindansalla()
        {
            int ilkDeger;
            int ikinciDeger;
            int sallamasyon;

            Console.WriteLine("İlk değeri girin: ");
            ilkDeger = int.Parse(Console.ReadLine());
            Console.WriteLine("ikinci değeri girin");
            ikinciDeger = int.Parse(Console.ReadLine());

            var rnd = new Random();


            //var birseyler = new test();
            //birseyler.test2(); /* eğer instance oluşturduysam*/
            //test.test1(); /*eğer instance oluşturulmadan kullanılmasını istediğim bir method var ise static yazılır.*/

            sallamasyon = rnd.Next(Math.Min(ilkDeger, ikinciDeger), Math.Max(ilkDeger, ikinciDeger));

            return sallamasyon;

        }
    }

    //class test
    //{
    //   public string Isim;
    //   public  string Soyad;

    //    public static void test1()
    //    {

    //    }

    //    public void test2()
    //    {

    //    }

    //}
}
